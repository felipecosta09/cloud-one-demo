#!/bin/bash
PERMITTEDHIGHVULNERABILITIES=0
OUT_OF_COMPLIANCE=0
curl -sk -X POST $DSSC_URL/api/sessions -H 'Content-type:application/json' -H 'X-Api-Version:2018-05-01' -d '{"user": {"userid":"'$DSSC_USER'","password":"'$DSSC_PSW'"}}'  > raw
cat raw | jq .token  > token
TEMP_TOKEN=`cat token`
sed -e 's/^"//' -e 's/"$//' <<< ${TEMP_TOKEN} > token
TOKEN=`cat token`
if [ -z ${TOKEN} ] ; then
  curl -sk -X POST $DSSC_URL/api/sessions -H 'Content-type:application/json' -H 'X-Api-Version:2018-05-01' -d '{"user": {"userid":"'$DSSC_USER'","password":"'$DSSC_PSW'"}}'  > raw
  cat raw | jq .token  > token
  TEMP_TOKEN=`cat token`
  sed -e 's/^"//' -e 's/"$//' <<< ${TEMP_TOKEN} > token
  TOKEN=`cat token`
fi
curl -sk -X POST $DSSC_URL/api/scans -H 'Content-type:application/json' -H "Authorization: Bearer ${TOKEN}" \
    -d '{
  "id": "",
  "name": "DSSC Scan Action",
  "source": {
    "type": "docker",
    "registry": "'$ECR_REPO'",
    "repository": "moneyx-app-protect",
    "tag": "latest",
    "credentials": {
      "token": "",
      "username": "",
      "password": "",
      "aws": {
        "region": "us-east-1",
        "accessKeyID": "'$AWS_ACCESS_KEY_ID'",
        "secretAccessKey": "'$AWS_SECRET_ACCESS_KEY'",
        "role": "",
        "externalID": "",
        "roleSessionName": "",
        "registry": ""
      }
    },
    "insecureSkipVerify": true,
    "rootCAs": ""
  }
} ' | jq .href > href || exit -10
